<?php
namespace App\Helpers;

use App\Models\PsOutputRow;

/**
 * 
 */
class ShellOuputMapper
{
	/**
	 * Map stdout to column array
	 * @param  string $output
	 * @return array
	 */
	private static function mapOutputToColumns(string $output) {
		$rows = explode(PHP_EOL, $output);

		array_shift($rows); //remove headers

		$data = [];
		foreach ($rows as $row) {
			$columns = [];
			$column_data = explode(' ', $row);
			foreach ($column_data as $col) {
				if (strlen($col) > 0) $columns[] = $col;
			}
			$data[] = $columns;
		}
		return $data;
	}

	/**
	 * Map output of the `ps -ao uid,pid` command to objects
	 * @param  string $output command output
	 * @return \Illuminate\Support\Collection
	 */
	public static function mapFunc2Output(string $output) {
		$rows = ShellOuputMapper::mapOutputToColumns($output);
		$data = [];
		foreach ($rows as $columns) {
			if (sizeof($columns) < 8) continue;
			$data[] = new PsOutputRow($columns[0], $columns[1], $columns[2], $columns[3], $columns[4], $columns[5], $columns[6], implode(' ', array_slice($columns, 7)));
		}

		return collect($data);
	}

	/**
	 * Map output of the `ps -ao uid,time,cpu` command to objects
	 * @param  string $output command output
	 * @return \Illuminate\Support\Collection
	 */
	public static function mapFunc3Output(string $output) {
		$rows = ShellOuputMapper::mapOutputToColumns($output);
		$data = [];
		foreach ($rows as $columns) {
			if (sizeof($columns) < 3) continue;
			$data[] = new PsOutputRow($columns[0], null, null, null, null, null, $columns[1], null, $columns[2]);
		}

		return collect($data);
	}
}