<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use Symfony\Component\Process\Process;

class Func1Command extends BaseCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'function:what-is-ps';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Output details about "ps" funtion';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $this->question('What is `ps` function/command?');
       $this->info('The ps utility displays a header line, followed by lines containing information about all of your processes that have controlling terminals. -from: "man ps"');
       $this->line('');
       $this->question('Functionalities of `ps` function/ command');
       $headers = ['Option', 'Description'];
       $data = [
            ['-A', 'Display information about other users\' processes, including those without controlling terminals.'],
            ['-a', 'Display information about other users\' processes as well as your own.  This will skip any processes which do not have a controlling terminal, unless the -x option is also specified.'],
            ['-C', 'Change the way the CPU percentage is calculated by using a ``raw\'\' CPU calculation that ignores ``resident\'\' time (this normally has no effect).'],
            ['-c', 'Change the ``command\'\' column output to just contain the executable name, rather than the full command line.'],
            ['-d', 'Like -A, but excludes session leaders.']
       ];
        $this->table($headers, $data);
        $this->info('ps contains lot of options to retrieve sort results');
        $this->askForInput();
    }

    protected function askForInput() {
        $input = $this->anticipate('What do you want to do next ("exit" or "more")?', ['exit', 'more']);
        if ($input == 'exit') {
            $this->getExitInput('askForInput');
        } else if ($input == 'more') {
            $process = new Process(['man', 'ps']);
            $process->run();
            $this->info($process->getOutput());
            $this->getExitInput('askForInput');
        } else {
            $this->info('Invalid input');
            $this->askForInput();
        }
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
