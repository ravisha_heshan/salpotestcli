<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use Symfony\Component\Process\Process;
use App\Services\CommandService;

class Func2Command extends BaseCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'function:users-who-have-launched-at-least-two-procs';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Get users who have launched at least two procs';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $process = new Process(['ps', '-af']);
        $process->run();
        $uidsOfUsersWhoHaveLaunchedAtLeastTwoProccessors = CommandService::findUsersWhoHaveLaunchedAtLeastTwoProcs($process->getOutput());

        if ($uidsOfUsersWhoHaveLaunchedAtLeastTwoProccessors->isEmpty()) {
            $this->info('No one launched at least two proccessors');
        } else {
            $headers = ['Username/ UID', 'No of processes launched'];
            $data = [];
            foreach ($uidsOfUsersWhoHaveLaunchedAtLeastTwoProccessors as $key => $result) {
               $data[] = [$key, $result];
            }
            $this->question('Users who have launched at least two proccessors');
            $this->table($headers, $data);
        }
        $this->getExitInput();
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
