<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use Symfony\Component\Process\Process;

class StartCommand extends BaseCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'start';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Start Application';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $option = $this->menu('List of functions', [
            'What is ps function?',
            'Users who have launched at least two processess',
            'Users who has used the most CPU resources',
            'Top 10 resource users',
            'Exit Application',
        ])
        ->disableDefaultItems()
        ->open();

        switch ($option) {
            case 0:
                $this->call('function:what-is-ps');
                break;
            case 1:
                $this->call('function:users-who-have-launched-at-least-two-procs');
                break;
            case 2:
                $this->call('function:users-who-have-used-most-cpu-resources');
                break;
            case 3:
                $this->call('function:top-resource-users-live');
                break;
            case 4:
                $this->notify("Bye", "Thank You 👋");
                break;
            default:
                $this->error('Invalid input');
                $this->getExitInput();
                break;
        }
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
