<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class BaseCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'base-command';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Base command';

    protected function getExitInput($callback = null) {
        if ($this->confirm('Go back to menu?')) {
            $this->call('start');
        } else {
            if (!is_null($callback)) {
                call_user_func([$this, $callback]);
            } else {
                $this->getExitInput();
            }
        }
    }
}
