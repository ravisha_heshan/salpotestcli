<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessTimedOutException;
use ProcessBuilder;

class Func4Command extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'function:top-resource-users-live';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Watch for top 10 resource users';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $process = new Process("top -o cpu -n 10 -stats user,cpu");
        $process->setTimeout(10); //1 hour
        $this->runProcess($process);
    }

    private function runProcess($process) {
        try {
            $process->run(function ($type, $buffer) {
                $this->showOutput($buffer);
            });
        } catch (ProcessTimedOutException $e) {
            $this->runProcess($process);
        }
    }

    private function showOutput($buffer) {
        system("clear");
        $this->question("Top 10 resource users");
        $this->info($buffer);
        $this->info('Use ^C to exit');
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
