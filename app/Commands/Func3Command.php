<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use Symfony\Component\Process\Process;
use App\Services\CommandService;

class Func3Command extends BaseCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'function:users-who-have-used-most-cpu-resources';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Get users who have used most cpu resources';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $process = new Process(['ps', '-ao', 'uid,time,cpu']);
        $process->run();
        $uidsOfUsersWhoHaveUsedMostCpu = CommandService::findUsersWhoHaveUsedMostCpuResources($process->getOutput());

        $headers = ['Username/ UID', '%CPU * time(mils)'];
        $data = [];
        foreach ($uidsOfUsersWhoHaveUsedMostCpu as $key => $result) {
           $data[] = [$key, $result->sum('cpu_usage')];
        }
        $this->question('Users who have used most cpu resources');
        $this->table($headers, $data);
        $this->getExitInput();
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
