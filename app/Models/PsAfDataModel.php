<?php
namespace App\Models;

/**
 * 
 */
class PsOutputRow
{
	/**
	 * UNIX identifier
	 * @var string
	 */
	public $uid;

	/**
	 * Process ID number
	 * @var string
	 */
	public $pid;

	/**
	 * ID number of the process′s parent process
	 * @var string
	 */
	public $ppid;

	/**
	 * CPU usage and scheduling information
	 * @var string
	 */
	public $c;

	/**
	 * Time when the process started
	 * @var string
	 */
	public $stime;

	/**
	 * Terminal associated with the process
	 * @var string
	 */
	public $tty;

	/**
	 * The amount of CPU time used by the process
	 * @var string
	 */
	public $time;

	/**
	 * Name of the process, including arguments, if any
	 * @var string
	 */
	public $cmd;

	/**
	 * Cpu load
	 * @var string
	 */
	public $cpu;

	function __construct($uid = null, $pid = null, $ppid = null, $c = null, $stime = null, $tty = null, $time = null, $cmd = null, $cpu = null)
	{
		$this->uid = $uid;
		$this->pid = $pid;
		$this->ppid = $ppid;
		$this->c = $c;
		$this->stime = $stime;
		$this->tty = $tty;
		$this->time = $time;
		$this->cmd = $cmd;
		$this->cpu = $cpu;
	}
}