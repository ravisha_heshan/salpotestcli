<?php
namespace App\Services;

use App\Helpers\ShellOuputMapper;

/**
 * 
 */
class CommandService
{
	/**
	 * Find user ids who have launched at least two proccessors using given input
	 * @param  string $command_output
	 * @return \Illuminate\Support\Collection
	 */
	public static function findUsersWhoHaveLaunchedAtLeastTwoProcs($command_output) {
		$outputData = ShellOuputMapper::mapFunc2Output($command_output);
        return $outputData->groupBy(function ($x) { 
                return $x->uid; 
            })
            ->filter(function($x) { 
                return sizeof($x) >= 2; 
            })
            ->map(function($x) { 
                return sizeof($x); 
            })
            ->sort()
            ->reverse();
	}

	/**
	 * Find user ids who have used most cpu resources using given input
	 * @param  string $command_output
	 * @return \Illuminate\Support\Collection
	 */
	public static function findUsersWhoHaveUsedMostCpuResources($command_output) {
		$outputData = ShellOuputMapper::mapFunc3Output($command_output);
        return $outputData->map(function($x) {  
                $mins = explode(':', $x->time)[0];
                $seconds = explode('.', explode(':', $x->time)[1])[0];
                $mils = (($seconds + ($mins * 60)) / 100) + explode('.', explode(':', $x->time)[1])[1];
                $x->cpu_usage = $mils * (($x->cpu == 0) ? : $x->cpu);
                return $x;
            })
            ->groupBy(function ($x) { 
                return $x->uid; 
            })
            ->sortByDesc(function ($x) {
                return $x->sum('cpu_usage');
            });
	}
}